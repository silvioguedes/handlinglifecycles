package com.silvioapps.handlinglifecyclessample;

import android.arch.lifecycle.LifecycleFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MainFragment extends LifecycleFragment {
    private MyLifecycleObserver myLifecycleObserver = null;


    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        if(getActivity() != null) {
            myLifecycleObserver = new MyLifecycleObserver(getActivity(), getLifecycle());
            getLifecycle().addObserver(myLifecycleObserver);
        }

        return layoutInflater.inflate(R.layout.activity_main, viewGroup, false);
    }

    @Override
    public void onStart(){
        super.onStart();

        if(myLifecycleObserver != null) {
            myLifecycleObserver.enable();
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        if(myLifecycleObserver != null) {
            myLifecycleObserver.disable();
        }
    }
}
