package com.silvioapps.handlinglifecyclessample;

import android.arch.lifecycle.LifecycleActivity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

public class MainActivity extends LifecycleActivity {
    private MyLifecycleObserver myLifecycleObserver = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myLifecycleObserver = new MyLifecycleObserver(this, getLifecycle());
        getLifecycle().addObserver(myLifecycleObserver);

        //descomente caso queir testar o fragmento
        /*if(savedInstanceState == null) {
            attachFragment(R.id.frameLayout, new MainFragment());
        }*/
    }

    @Override
    public void onStart(){
        super.onStart();

        if(myLifecycleObserver != null) {
            myLifecycleObserver.enable();
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        if(myLifecycleObserver != null) {
            myLifecycleObserver.disable();
        }
    }

    protected void attachFragment(int resId, Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment fragmentById = getSupportFragmentManager().findFragmentById(resId);
        if(fragmentById == null && !isFinishing()){
            fragmentTransaction.add(resId, fragment);
            fragmentTransaction.commit();
        }
    }
}
