package com.silvioapps.handlinglifecyclessample;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;
import android.widget.Toast;

/**
 * Created by silvio on 9/5/17.
 */

public class MyLifecycleObserver implements LifecycleObserver{
    private Context context = null;
    private Lifecycle lifecycle = null;

    public MyLifecycleObserver(Context context, Lifecycle lifecycle){
        this.context = context;
        this.lifecycle = lifecycle;
    }

    //só será habilitado após o onCreate()
    public void enable(){
        if(lifecycle != null && lifecycle.getCurrentState().isAtLeast(Lifecycle.State.CREATED)) {
            Toast.makeText(context, "Enabled", Toast.LENGTH_SHORT).show();
        }
    }

    //só será desabilitado após o onDestroy()
    public void disable(){
        if(lifecycle != null && lifecycle.getCurrentState().isAtLeast(Lifecycle.State.DESTROYED)) {
            Toast.makeText(context, "Disabled", Toast.LENGTH_SHORT).show();
        }
    }

    //método chamado automaticamente no onCreate() da atividade/fragmento
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    public void create(){
        Toast.makeText(context, "Created", Toast.LENGTH_SHORT).show();
    }

    //método chamado automaticamente no onStart() da atividade/fragmento
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void start(){
        Toast.makeText(context, "Started", Toast.LENGTH_SHORT).show();
    }

    //método chamado automaticamente no onResume() da atividade/fragmento
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void resume(){
        Toast.makeText(context, "Resumed", Toast.LENGTH_SHORT).show();
    }

    //método chamado automaticamente no onPause() da atividade/fragmento
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void pause(){
        Toast.makeText(context, "Paused", Toast.LENGTH_SHORT).show();
    }

    //método chamado automaticamente no onStop() da atividade/fragmento
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void stop(){
        Toast.makeText(context, "Stopped", Toast.LENGTH_SHORT).show();
    }

    //método chamado automaticamente no onDestroy() da atividade/fragmento
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void destroy(){
        Toast.makeText(context, "Destroyed", Toast.LENGTH_SHORT).show();
    }
}
